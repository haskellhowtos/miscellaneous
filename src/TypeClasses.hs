{-# LANGUAGE TypeFamilies #-}
module TypeClasses
  where

-- TODO: check how to import the strict versions
import Data.Map (Map)
import qualified Data.Map as Map

-- |Explicit implementation of a type classes and their dictionaries
-- This is a toy implementation for a type class Pos representing positive
-- numbers on which addition and multiplication is defined. A type class
-- declaration would have to define a data-type for a @Pos@ dictionary
-- like this:
data PosDict a = MkPos { plus :: a -> a -> a , times :: a -> a -> a }
-- |For every datatype that is an instance of @Pos@, a constructor
-- is defined, for Double this would look like

posDoubleD :: PosDict Double
posDoubleD = MkPos (+) (-)

-- | For Int it would be like
posIntD :: PosDict Int
posIntD = MkPos (+) (-)

-- |A polymorphic function corresponding to @Pos a => a -> a@. Due to the
-- Haskell type system this requires a Num because 1 is a Num, and so are
-- (+), (-)

inc :: Num a => PosDict a -> a -> a
inc d x = (plus d) x 1

-- | Fom this, @inc posDoubleD :: Double -> Double@ and
-- @inc posIntD :: Int -> Int@
