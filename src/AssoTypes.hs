-- Experiments in associated datatypes
{-# LANGUAGE FlexibleInstances #-} -- for instantiating type-classes for phantom types
{-# LANGUAGE MultiParamTypeClasses #-} 
{-# LANGUAGE KindSignatures #-} --needed for associated datatypes
{-# LANGUAGE TypeFamilies #-} --needed for associated datatypes
{-# LANGUAGE DataKinds #-} 
-- {-# LANGUAGE ExistentialQuantification #-} 
-- {-# LANGUAGE RankNTypes #-} 
-- {-# LANGUAGE StandaloneDeriving #-} -- We want to derive @Show@ instances for GADTs necessary for REPL use.

module AssoTypes where
-- |This is an attempt to have flexible sets where t is a type, such as
-- Plain, Sortable etc along the lines of FSet in Gadts.hs but where the
-- constructors are defined in instances of FlexSets. Thus, the t is like
-- a phantom type, and this thing could be used in cases where class instances
-- of phantom types are used. The Normalizer is a type-level function that
-- returns a normalized type, for example a SortableSet would normalize to an
-- OrderedSet
class FlexSets t c where
  type Normalizer t :: *
  -- Normalizer t = t -- default implementation doesn't compile
  data FlexSet t c :: *
  member :: c -> FlexSet t c -> Bool
  normalise :: FlexSet t c -> FlexSet (Normalizer t) c
-- normalise = id -- doesn't work either, would need a default for Normalizer
-- Note that FlexSet t c -> FlexSet n c wouldn't work
-- a return type of FlexSet n c would mean
-- that the caller determines the n and can choose whatever n,
-- returning a FlexSet Plain wouldn't be possible because that would constrain
-- n to Plain by the provider, not the caller.

-- |This is not a good idea, it fixes the SetTypes to the ones provided here.
data SetType = Plain | Sortable | Ordered
-- |For the @normalise@ function we need  a signature
-- ∀ c (∀ t ∃ n).FlexSet t c -> FlexSet n c
-- Haskell has no ∃, so we have to use skolemization, ie a function sk(t)
-- returning an @n@ that exists for each @t@, so the skolemized signature
-- of @normalise@ is @normalise::∀ c,t.FlexSet t c -> FlexSet sk(t) c@ where
-- sk is the @SetNormalizer@ below:
-- Again, this type family should be open so that it can be extended as new
-- set-types crop up.
type family SetNormalizer a::SetType where
  SetNormalizer 'Sortable = 'Ordered
  SetNormalizer a = a
--  SetNormalizer 'Ordered = 'Ordered
--  SetNormalizer 'Plain = 'Plain

-- |There should be a functional dependency c -> t because the type of the
-- set depends on its content. The problem is that someone could write
-- instances of Ordered where the set is not ordered

data PlainSet
instance Eq c => FlexSets PlainSet c where
  type Normalizer PlainSet = PlainSet
  data FlexSet PlainSet c = FlexP [c]
  member x (FlexP l) = elem x l
  normalise  = id
  
orderFlex :: FlexSet SortableSet c -> FlexSet OrderedSet c
orderFlex=undefined

data SortableSet
data OrderedSet
instance Ord c => FlexSets SortableSet c where
  type Normalizer SortableSet = OrderedSet
  data FlexSet SortableSet c = FlexS [c]
  member x (FlexS l) = elem x l
  normalise = orderFlex
  
instance Ord c => FlexSets OrderedSet c where
  type Normalizer OrderedSet = OrderedSet
  data FlexSet OrderedSet c
  member = undefined
  normalise = id
