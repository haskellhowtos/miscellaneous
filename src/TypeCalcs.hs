{-# LANGUAGE DataKinds #-}      --needed for type arithmetic, with this every
                                --type gets an associated kind and every data
                                --constructor C gets an associated type
                                --constructor C'
{-# LANGUAGE KindSignatures #-} --needed for type arithmetic, with this every
                                --type can be annotated with its kind
{-# LANGUAGE TypeOperators #-}  --needed to use infix operators on the type
                                --level, such as ':, + from GHC.TypeLits et al
{-# LANGUAGE TypeFamilies #-}   --needed to define functions on the type level
{-# LANGUAGE PolyKinds #-}      --needed for polymorphic type level functions 
{-# LANGUAGE UndecidableInstances #-} --needed for recursive definitions of
                                      --type level functions
{-# LANGUAGE ScopedTypeVariables #-} --makes type variables in type annotations
                                     --available in the corresponding term
{-# LANGUAGE GADTs #-}
module TypeCalcs where
import GHC.TypeLits (type (+), Nat)

-- There are two types of type families: open and closed, the ones below
-- are all closed because of the @where@ keyword.
-- |A type level IF of kind Bool -> * -> * -> *
type family IF c t f where
  IF 'True t f = t
  IF 'False t f =f
-- |A comparison of kind Ordering -> * -> * -> * ->*
-- Meant for use with CmpNat, which returns an Ordering rather than a Boolean
type family Cmp o l e g where
  Cmp 'LT l e g = l
  Cmp 'EQ l e g = e
  Cmp 'GT l e g = g
-- |List type-level functions
-- Length [Char,Bool,Int]
-- Length needs PolyKinds otherwise Length [1,2,3] wouldn't compile
-- because 1,2,3 are all Nat but not *. Note that the k in the kind annotation
-- is a kind variable, [*] wouldn't work here. Length also needs
-- UndecidableInstances because of the recursive definition.
type family Length (ls::[k]) where
  Length '[] = 0
  Length (x ': ls) = 1 +Length ls

-- |Note that Head '[] returns a valid type.
type family Head (ls :: [k]) where
  Head (x ': ls) = x
-- |Need a branch for empty lists?
type family Tail (ls :: [k]) where
  Tail (x ': xs) = xs
  Tail '[] = '[]

-- |Type-level mapping
-- Note f :: * -> * or k -> k or k -> * would be too restrictive,
-- k -> l is needed to run MakeTuple on it, ie need two kind variables.
type family Map (f :: k -> l) (xs :: [k]) where
  Map f '[] = '[]
  Map f (x ': xs) = f x ': Map f xs
  
-- |This can be used with something like MakeTuple. Note that
-- this can not be made polymorphic by replacing * with k because
-- of unification problems (?). Using '(,) doesn't help because
-- this would make it a k -> (k,k) but a k -> k is needed for polymorphism
-- which can only be the identity (? even for kinds ?)
-- MakeTuple can be polymorphic of kind k -> (k,k) 
type family MakeTuple (x :: k) where
  MakeTuple x = '(,) x x --this doesn't work, has kind k -> (k,k)
  -- MakeTuple x = (x , x) -- this has kind * -> * as required
  
-- and then
-- Map MakeTuple [Char,Bool,Int] should return [(Char,Char),(Bool,Bool),(Int,Int)
