module MonadEval
  where

-- |In Haskell, types are defined with the @data@ keyword, product
-- types can be written as tuples. All types and constructors in
-- Haskell have to start with upper case letters.
data Term = Cons Int | Div (Term,Term)

evalRaw :: Term -> Int
evalRaw = undefined

-- | does this make sense? problem with a double recursion?
-- this should take into account previous failures, somehow a
-- recursive application of evalRaw
evalSafe :: Term -> Either String Int
evalSafe (Div (x,y))| evalRaw y==0 = Left "Division by zero"
                    | otherwise = Right (div (evalRaw x) (evalRaw y))
-- |To handle division by zero and issue a message, the @Either@ type
-- functor is used to construct the return type of the evaluation function
-- It is defined as
-- @data Either a b = Left a|Right b
-- The data functor @Either a@ is already a monad in Haskell, so the
-- binding operator >>= is already defined. Note that in Haskell nameless
-- functions are introduced with the @\args -> body@ notation.
eval :: Term -> Either String Int
eval (Cons x) = Right x
eval (Div (x,y)) = eval x >>=
                    \fa ->
                      eval y >>= \sa ->
                        if sa == 0
                        then Left "Division by zero"
                        else Right (div fa sa)
-- |The nested application of the binding operator @>>=@ combined with
-- the nameless functions are often avoided using @do@ notation as in
eval' :: Term -> Either String Int
eval' (Cons x) = Right x
eval' (Div (x,y)) = do
  fa <- eval x
  sa <- eval y
  if sa == 0
  then Left "Division by zero"
  else Right $ div fa sa

-- |This style looks more like imperative programming so opinions about it
-- are divided, but once there are more than two nested applications of
-- @>>=@, the binding notation becomes really messy. Another way
-- around this is the introduction of named functions.
eval'' :: Term -> Either String Int
eval'' (Cons x) = Right x
eval'' (Div (x,y)) = eval x >>= safeDivisionBy y
  where
    safeDivisionBy den nm = eval den >>= \sa ->
                        if sa == 0
                        then Left "Division by zero"
                        else Right (div nm sa)

-- |Using state to count divisions. Note that @return@ is the Haskell
-- name for the monadic unit so @return x@ just packs the value into
-- into the @State@ monad, it has nothing to do with returning values
-- in the traditional sense. Some freaky sense of humor at work here.
-- Might be easier to have evalS return (st,rslt) and use @state evalS@?
-- Check this out: we should use a raw eval and then just extend it with
-- a logging functions or some such
evalS :: Term -> State Int Int
evalS (Cons x) = return x
evalS (Div (x,y)) = evalS x >>= countDivs
 where
   (dcount, rslt) = runState (evalS y) 0
   countDivs nm = put (dcount + get + 1) >> return $ div nm rslt
