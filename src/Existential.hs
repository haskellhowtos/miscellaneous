{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}

module Existential
where

-- TODO: a datatype to pack angle values existentially, ie its value
-- should only be accessible through the trig functions because there
-- the value only matters modulo 2π. There needs to be arithmetic and
-- function access to the hidden value.

-- this is a first attempt at working with a hidden angle value
-- all the trig functions are Floating a => a -> a
-- Note:this isn't existential, this is just a type annotation
-- for @angle@ using an explicit @forall@.
newtype AngleValue = AngleValCon { angle ::forall a.Floating a => a }
                        -- deriving Num --doesn't work
-- This is existential and usable because @Real@ a has a conversion
-- @toRational@ which disconnects the hidden value from its unknown
-- data-type to a @Rational@, which can then be used outside. It
-- also can be repolymorphised to a @Fractional a@ using @fromRational@
-- in the result of @toRational@.
data AngleValue1 = forall a. Real a => AngleValue1Con {angle1 :: a}
data AngleValue2 b = forall a. Floating a => AngleValue2Con {angle2 :: a
                                                           , with :: a -> b
                                                          }
-- This compiles but cannot be applied  to a function like tan,
-- probably because it is too general, it would perhaps work
-- with a tan :: Floating a => a -> Double because that way the
-- type wouldn't escape
-- withAV :: AngleValue1 ->forall r.(forall a. Floating a =>a -> r) ->r
-- withAV (AngleValue1Con av) f = f av

add (AngleValCon v1) (AngleValCon v2) = AngleValCon (v1+v2)
-- This doesn't work, type variable escapes its scope because
-- tan :: (Floating a) => a -> a so the right hand side has the same unknown
-- type as the left hand x
-- tangent1 (AngleValue1Con x) = tan x
-- but this compiles:
-- tangent1 (AngleValue1Con x) = AngleValue1Con $ tan x
-- but doesn't make sense because the result is not an angle
-- this compiles if the existential variable is constrained to be Real because
-- then it can be converted to rational, and trig functions work on Fractionals
trig1 :: Fractional t =>AngleValue1 -> (t -> p) -> p
trig1 (AngleValue1Con x) tf = tf $ fromRational $ toRational x
-- tangent2 :: AngleValue2Old -> Double
--tangent2 (AngleValue2ConOld v) = toRational $ tan v
-- The only way to use this is with an internal function
tangent2 (AngleValue2Con v tan) = tan v
-- inc (AngleValue2Con v add1) = add1 v
 --where
   --add1 :: Floating a => a -> a
   --add1=(1+)
data family NumDict a
data instance NumDict Double = MkNumDD { plus :: Double -> Double -> Double
                                       , times :: Double -> Double  -> Double}
data NumDictFctns = Sum|Product
-- Here we can add other instances, problem is: plus, times need to be
-- different, need a dictionary String -> Function so "plus" can be retrieved
-- by name etc?
-- For NumDict create an enumeration type with the names of the functions
-- as constructors, then a dictionary with the enumeration as keys.
-- use Data.Map from containers, import Data.Map (Map) and qualified Data.Map
-- as Map
