{-# Language DataKinds #-}
{-# Language GADTs #-}
{-# Language KindSignatures #-}
{-# Language RankNTypes #-}
{-# Language LambdaCase #-}
{-# Language TypeApplications #-}
{-# Language TypeFamilies #-}
-- this includes code from
-- https://blog.jle.im/entry/introduction-to-singletons-1.html
-- an introduction to singletons and the singleton library.
module Singles where

-- | The state of doors.
-- The DataKinds extension provides a kind @DoorState@ in addition to
-- a type @DoorState@ as well as types @'Opened@, @'Closed@ and @'Locked@.
data DoorState = Opened | Closed | Locked
  deriving (Show, Eq)
-- |As a consequence, we can do things like this:
data Door (s :: DoorState) = UnsafeMkDoor { doorMaterial :: String }
-- where the kind of the type s is restricted to @DoorState@, hence it
-- can assume only type values @'Opened@, @'Closed@ and @'Locked@.

-- |A singleton type based on the kind @DoorState@. Each constructor returns
-- a different type with one value. Compare this to enumerations
-- data DoorStatesEnum=Opened|Closed|Locked where all three constructors
-- construct a value of type DoorStatesEnum.
data SingDS :: DoorState -> * where
    SOpened :: SingDS 'Opened
    SClosed :: SingDS 'Closed
    SLocked :: SingDS 'Locked

fromSingDS :: SingDS s -> DoorState
fromSingDS SOpened = Opened
fromSingDS SClosed = Closed
fromSingDS SLocked = Locked

-- |An example of reflection:
-- Turning type variable @s@ into a runtime value of Type DoorState
doorStatus :: SingDS s -> Door s -> DoorState
doorStatus s _ = fromSingDS s
