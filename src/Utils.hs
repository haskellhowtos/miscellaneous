-- a collection of common algorithms and how they would be implemented in
-- Haskell, some of them in different ways
module Utils where

---------------------Maxima and Minima-----------------------------------
-- | maximum of a list, raw recursion, only on nonempty lists
maxR :: Ord t => [t] -> t
maxR (x:xs) = cmpFst x xs
  where
    cmpFst x [] = x
    cmpFst x (y:ys)
       | x < y = cmpFst y ys
       |otherwise = cmpFst x ys

-- | more generally, comparison is replaced by an @f::t -> t -> Bool@.
minmax :: (t -> t -> Bool) -> [t] -> t
minmax f (x:xs) = cmpFst x xs
 where
   cmpFst x [] = x
   cmpFst x (y:ys)
       | f x y = cmpFst y ys
       | otherwise = cmpFst x ys
       
-- | now min and max can be defined via currying from minmax
maxL :: Ord a => [a] -> a
maxL = minmax (<)

minL :: Ord a => [a] -> a
minL = minmax (>)

-- | maximum using a left fold, note this is partial, no empty lists
maxfold (x:xs) = foldl comp x xs
  where
    comp a b
      |a < b = b
      |otherwise = a
------------------------Subsequences---------------------------------------

-- | TODO
-- longest increasing subsequence
-- longest :: [a] -> [a] -- or should we use a traversable t a -> t a?
-- this is probably best done with a fold which keeps track of
-- the old starting index of the previous longest sequence, its length,
-- the last value from the list the last index from the list, the start
-- index of the current increasing sequence and its length so far
-- Note that seqTracker doesn't move a final sequence to the front, so this
-- has to be done separately. Also, what is the longest subsequence in
-- [1,2,1,2,3,1,2,3,4]? Of course [1,2,3,4] but based on what?
-- consider [1,2,1,3,1,4]: this is also [1,2,3,4], so this needs more
-- clarification
longestInc (x:xs) = out
  where
    (os,ol,_,_,cs,cl) = foldl seqTracker (0,0,x,0,0,0) xs
    -- start= if ol > cl then os else cs
    -- length = if ol > cl then ol else cl
    out = if ol > cl then (os,ol) else (cs,cl)
 --Note: seqTracker won't move last subsequence to front, this has to be
 -- done by the caller
 -- seqTracker on old index, old length, last value, last index, current base
 -- index, current length (ie start index of current sequence, length of
 -- current sequence)
seqTracker (oidx,ol,lval,lidx,cidx,cl) y
  | y > lval = (oidx,ol,y,lidx+1,cidx,cl+1) --increase continues
  | otherwise = if ol < cl --increase stops
                then (cidx,cl,y,lidx+1,lidx+1,1) --new longest sequence

                else (oidx,ol,y,lidx+1,lidx+1,1) --old sequence still longest
--Same thing here, this time actually collecting sublist instead of merely
--startindex and length. This should be equivalent to
--subList (take (snd $ longestInc l) [fst (longestInc l),..]) l
longestIncList (x:xs) = out
  where
    (os,ol,_,_,cs,cl) = foldl seqSubList ([],0,x,0,[],0) xs
    out = if ol > cl then os else cs
-- works like seqTracker except for collecting lists instead of merely
-- keeping start index and length.
seqSubList (olst, ol, lval, lidx, clst,cl) y
  | y > lval =(olst,ol, y, lidx+1, clst ++ [y],cl+1)
  |otherwise = if ol < cl
               then (clst, cl, y, lidx+1,[y],1)
               else (olst,ol,y,lidx+1,[y],1)
-- | Creating  a sublist from indices, indices must be sorted in ascending
-- order, such as returned by @findIndices@.
-- perhaps use something like this:
-- newtype Sorted a = MkSorted [a] deriving ..., we'd need a fromList, toList
-- conversion.
-- TODO:this should probably be done by a handmade recursion because once the
-- index list is empty, the whole procedure can be aborted
subListFold [] l = []
subListFold idcs l = proj1 $ foldl idxCheck ([],0,idcs) l
  where
    idxCheck (subs, cidx, []) le = (subs,cidx,[]) --could actually stop here
    idxCheck (subs, cidx, cidcs@(idx:idcs)) le =
      if cidx==idx
      then (subs ++ [le], cidx+1, idcs)
      else (subs, cidx+1, cidcs)
    proj1 (x,_,_) = x

--subList from indices, raw recursion, still needs sorted index list
--Advantage over subListFold: this stops when index list is empty, which should
--happen when the index list is in the range of the indices of the target
--list. Unless the last index in the index list refers to the last element
--in the target list, in which case both lists are empty in the same step.
--So it doesn't have to continue until the index list is exhausted as would
--be the case with subListFold. This should improve performance when extracting
--a sublist at the beginning of a huge target list.
subList [] l = []
subList idcs l = subListRec idcs 0 [] l
  where
    subListRec [] _ s _ = s --running out of sublist indices
    subListRec _ _ s [] = s --running out of list
    subListRec idcs@(idx:idxs) cidx s (le:ls)
      |idx==cidx = subListRec idxs (cidx+1) (s ++ [le]) ls
      |otherwise = subListRec idcs (cidx+1) s ls
-- |general increasing sub lists, ie not necessarily with adjacent elements
-- increasing, for example [1,2,1,3,1,4] has increasing subsequence
-- 1,2,3,4 at indices 0,1,3,5
incListGen l@(lh:lt) = incListGenRec 0 (subSeq 0 l) l
   where
     incListGenRec _ sub [] = sub
     incListGenRec bix sub cl@(_:clt)
       | length sub >= length cl = sub -- no longer sublist possible
       | length newsub > length sub = incListGenRec (bix+1) newsub clt
       | otherwise = incListGenRec (bix+1) sub clt
       where
         newsub=subSeq bix cl
         
-- | @subSeq idx lst@ looks for a strictly increasing subsequence
-- of @lst@ setting @head lst@ to index @idx@
-- look for an increasing sequence based on index bix and check if it is
-- longer than the current longest subsequence and report to the next
-- level
subSeq :: (Ord c, Num a) => a -> [c] -> [a]
subSeq _ [] = []
subSeq bidx (flh:flt) = proj1 $ foldl folder ([bidx],bidx+1,flh) flt
  where
     -- this looks for an increasing subsequence of fl
     folder (lidx,idx,val) y|y>val = (lidx ++ [idx],idx+1,y)
                            |otherwise = (lidx,idx+1,val)
     proj1 (x,_,_) = x
-- TODO: check call: this calculates the 0-based one twice
incListAll l = incListGenAll 0 [] l
  where
    incListGenAll _ subs [] = subs
    incListGenAll bix subs cl@(_:clt) = incListGenAll (bix+1) (subs ++ [newsub]) clt
      where
        newsub = subSeq bix cl
    
--this works correctly, bidx is the index of flh in the original list
--subSeq _ [] = []
--subSeq bidx (flh:flt) = proj1 $ foldl folder ([bidx],bidx+1,flh) flt
 --where proj1 (x,_,_)=x
 -- foldl folder ([bidx],bidx,head l) l works or
 -- foldl folder ([bidx],bidx+1, head l) (tail l)
 -- the result should always be
 -- (list, length of input list, last element of input)
-----------------------Permutation Containment---------------------------------
-- |Deltas: @if l!!i < l!!(i+1) then d!!i=1 else d!!i =-1@, considered
-- cyclically, ie last element of @l@ is compared to its head
deltas [] = []
deltas (lh:lt) = snd diffs ++ [hdLstDiff]
  where
    diffs = foldl diff (lh,[]) lt
    hdLstDiff | lh > fst diffs = 1
              | lh < fst diffs =(-1)
              |otherwise = 0
    diff (val,dl) y
      |val/=y = (y, dl ++ [if y>val then 1 else (-1)])
      |otherwise = (y,dl++[0]) --strictly speaking an error

-- | strictly speaking, we need to worry about equality which shouldn't
-- occur because l is supposed to be a permutation
deltasOps :: (Ord a1, Ord a) => [a] -> [a1 -> a1 -> Bool]
deltasOps [] = []
deltasOps (lh:lt) = snd diffs ++ [hdLstDiff]
  where
    hdLstDiff | lh > fst diffs = (<)
              | lh < fst diffs = (>)
              |otherwise = (==)
    diffs = foldl diff (lh,[]) lt
    diff (val,dl) y
      |val /= y = (y, dl ++ [if val < y then (<) else (>)])
      |otherwise = (y, dl ++ [(==)])
-- | Now we just need to generate all sublists of a certain length
-- and see whether their deltas are the same. Make sure this can be
-- interrupted as soon as it becomes clear that the deltas don't match
-- contained s l = should generate sublists of l of length s and compare their
-- deltas, need a searchDelta d inL fromIdx
-- contained (deltasOps sub) lst $head lst checks if lst contains sub base on
-- head lst, ie with head lst as first element of a possible sub list,. But
-- this should continue with its tail
containedAt :: [a -> a -> Bool] -> [a] -> a -> Bool
containedAt [] _ _ = True
containedAt _ [] _ = False --never happens by recursion
containedAt (dh:[]) (lh:_) fst  = dh lh fst --last operator, needs to match fst
containedAt (dh:_) [le] _ = False --at least 2 operators left at end of list
containedAt d@(dh:dt) l@(lh:ls:lt) fst
  | dh lh ls = containedAt dt (ls:lt) fst
  | otherwise = containedAt (dh:dt) (ls:lt) fst

-- This may be inefficient if the deltas are recomputed at every step, but
-- hopefully not because of purity
contained :: (Ord a1, Ord a2) => [a2] -> [a1] -> Bool
contained sub l@(lh:lt)
  |length sub > length l = False
  |containedAt ds l (lh) = True
  |otherwise = contained sub lt
  where ds = deltasOps sub

--TODO: would be nice to know which subsequences are similar to the given
--one, not only if there is one
--This one has to build up a list and pass it down, need to keep track
--of index of head
--TODO: this doesn't work yet correctly, check how indices are updated
containedAtIndices :: [a -> a -> Bool] -> [a] -> a -> Int -> [Int] -> [Int]
containedAtIndices [] _ _ _ idcs = idcs --TODO :probably should return empty
-- the base case: this returns the accumulated list if first and last index
-- match properly, operator list exhausted, so have to match with fst.
containedAtIndices (dh:[]) (lh:_) fst cidx idcs
  |dh lh fst = idcs ++ [cidx]
  |otherwise = []
containedAtIndices (dh:_) [le] _ _ _= [] --too many operators left
containedAtIndices d@(dh:dt) l@(lh:ls:lt) fst cidx idcs
  | dh lh ls = containedAtIndices dt (ls:lt) fst nidx (idcs ++ [cidx])
  | otherwise = containedAtIndices (dh:dt) (ls:lt) fst nidx idcs
  where nidx = cidx + 1
