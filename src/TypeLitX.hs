-- TypeLit examples, how to use GHC.TypeLits
-- See https://blog.jle.im/entry/fixed-length-vector-types-in-haskell.html
{-# LANGUAGE KindSignatures #-} --needed for type arithmetic
{-# LANGUAGE DataKinds #-}      --needed for type arithmetic
{-# LANGUAGE ScopedTypeVariables #-}      --needed for type arithmetic
{-# LANGUAGE TypeOperators #-}  --needed to use + on types
{-# LANGUAGE FlexibleContexts #-}  --dito
{-# LANGUAGE TypeFamilies #-}  --dito
{-# LANGUAGE PolyKinds #-} --using kind variables in type family Cmp
{-# LANGUAGE RankNTypes #-} --using kind variables in type family Cmp
{-# LANGUAGE TypeApplications #-} --using kind variables in type family Cmp

module TypeLitX where

import GHC.TypeLits (type (+), type (-))
import qualified GHC.TypeLits as TL --module implementing type arithmetic
import Data.Proxy (Proxy(..))

data List (n::TL.Nat) a = MkList { getList :: [a] }

-- |intelligent constructor calculating the length type from the length of the
-- argument.
-- mkList :: forall n a . TL.KnownNat n => [a] -> List n a
-- This is not going to work that way, List n [a] has to be existential as in
-- mkList :: [a] -> (forall n . TL.KnownNat n =>  List n a)
-- but then the existential escapes its scope
-- mkList l = case ll of
              -- Just (TL.SomeNat (Proxy::Proxy m))-> MkList @m l
              -- Nothing -> MkList @0 []
  -- where ll=TL.someNatVal $ toInteger $ length l

-- | It is impossible to create a List n a from a list [a] because the
-- length of it is unknown at compile time, the only thing that is known
-- at compile time is the existence of a length. The only way to use an
-- existential type is to apply a function to it, this is what withList
-- achieves
withList :: [a] -> (forall n . TL.KnownNat n =>  List n a -> r) -> r
withList l f  = case ll of
              Just (TL.SomeNat (Proxy::Proxy m))->  f (MkList @m l)
              Nothing -> f(MkList @0 []) 
  where ll=TL.someNatVal $ toInteger $ length l
-- or could we just mkList l = MkList l :: List l a
-- or mkList l = MkList :: List (Length l) a?

-- | Another possible use is to have an intelligent constructor that checks
-- the proper length? Again, the problem
-- seems that we're promising a List n a but the caller has no way of
-- choosing n, except if it is in the argument. This could be called
-- with mkList (fromInteger $ length x) x, roughly, there is an issue with
-- length being an integer.
-- This one leaves the decision to the caller because any n, a can be chosen,
-- the result is just a Nothing if things don't match, which is of type
-- Maybe (List n a)
mkList :: forall n a. TL.KnownNat n => [a] -> Maybe (List n a)
mkList l
  |length l == tl = Just $ MkList l
  |otherwise = Nothing
  where
    tl = fromIntegral (TL.natVal (Proxy @n))
mkEmpty = mkList @0 []

-- How about this?
-- mkListN ls = mkList @(Length ls) ls --no, this works only on list of
                                       -- datatypes
--This should work: check length and return Nothing in case of mismatch
--This could be called as mkListN (length l) l, but no difference,
--caller imposes length parameter, this works only for lists existant
--at compile time, ie constant lists
--mkListN :: forall n a. TL.KnownNat n => Int -> [a] -> Maybe (List n a)
